-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

annotations:
  artifacthub.io/changes: |
    - kind: added
      description: >-
        Block(data-loader): Add support for context. The data passed to the block
        (for example the current data in a flow page) is now available in the
        property data when using context remappers.
    - kind: added
      description: 'Block(video): Add video block.'
    - kind: added
      description: 'App: Add condition action.'
    - kind: added
      description: 'App: Add action flow.to.'
    - kind: added
      description: >-
        Appsemble: Add translate remapper. This can be used instead of
        message.format for simpler messages that don’t use any message values.
    - kind: added
      description: 'Cli: Add support setting an icon when creating an app.'
    - kind: added
      description: 'Cli: Add support setting an icon when updating an app.'
    - kind: added
      description: 'Cli: Set the process name to appsemble.'
    - kind: added
      description: 'Server: Add condition action.'
    - kind: added
      description: 'Server: Allow specifying iconBackground when creating an app.'
    - kind: added
      description: 'Server: Allow specifying maskableIcon when creating an app.'
    - kind: added
      description: 'Server: Set the process name to appsemble.'
    - kind: fixed
      description: >-
        App: Fix inclusion of context within remappers for query in request actions.
        This was causing context to not be available.
  artifacthub.io/license: LGPL-3.0-only
  artifacthub.io/links: |
    - name: Appsemble Studio
      url: https://appsemble.app
    - name: Documentation
      url: https://appsemble.app/docs
  artifacthub.io/recommendations: |
    - url: https://artifacthub.io/packages/helm/cert-manager/cert-manager
    - url: https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx
apiVersion: v2
appVersion: 0.18.16
dependencies:
- - condition: postgresql.enabled
  name: postgresql
  repository: https://charts.bitnami.com/bitnami
  version: 8.9.6
description: The open source low-code app building platform
home: https://appsemble.com
icon: https://charts.appsemble.com/icon.svg
keywords:
- - app
- - apps
- - appsemble
- - framework
- - low-code
- - lowcode
kubeVersion: '>=1.20.0'
name: appsemble
sources:
- - https://gitlab.com/appsemble/appsemble
- - https://gitlab.com/appsemble/charts
type: application
version: 0.18.16

...
files:
  appsemble-0.18.16.tgz: sha256:94dac2c4008d7e175317298ff220aa8d777067ec4f9f71b5fea5e42425db80b0
-----BEGIN PGP SIGNATURE-----

wsFcBAEBCgAQBQJgwd3VCRA0b/IkjzELUwAA0iUQACsdDfCNj/kcd15PTi7b91yM
MqEAWCIstzV1kP5tsqvNrR9RzaXUQg7OZcCwfKdlYId/+ESGvgbBSvylpBpCzSLy
+ptLddOmIjoNoDWliVOoYsqUa5sbze4MLdYiqbYx5zXQpgGqg//MYhsp5klwXEPb
+g12guVC3V69AZOZ3Y0uAvSb+kB3Yo+5azAub/5W6giNx9kER3vW4G8RmhGdGTKR
mwg9I5zyqQLJ6htKwfy/ZRh03ZZSlWLQBaObb89+whdN3dxQsborJAxHWwY0sa0N
ZBWjPmRl+M7iEBZFCNSYOLYj1u0rB9l3wxLMFwjyk7PAhkrCsOpyLFj1PeAo022L
MQzCPlVL9nAIVanheXCIgEDh4clV4LVJK484uYNsPs/vy0BBx0W36anEVgVGJydI
qe+1JxCCgvAtZodcCRosW6RcX8QGwIOjLWGwQ63KjbKPKoNziuobqQvhDtuI40w6
NuP8FvTtJ4KjIC9cPJcnT/6FiGQw/j5i6EDxr2175GJwrtQjBHYMFznBqTHVaNv7
CAbtPsLkTSsy4b9Y8bHB905LSz9opzuY5QZXDBde6JOqBwSfNidz0alG/UCBnlbt
o9BPxQbHgnQ596+s/F/dtWkC0Nv0oWLzFvqZBRZS557R+PJEwBqD9earKtIKbUj/
eZlScMhDKoccb7o7s3uG
=CV/P
-----END PGP SIGNATURE-----