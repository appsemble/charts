-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

annotations:
  artifacthub.io/changes: |
    - kind: added
      description: "Block(form): Use the accept requirement of input fields to show an
        icon in the upload button."
    - kind: added
      description: "Block(list): Show the filename and filetype icon in the header of
        list items, with a header value remapping to an asset id."
    - kind: added
      description: "App: Unregister service worker in app debug page."
    - kind: added
      description: "Server: Add Access-Control-Expose-Headers: 'Content-Disposition'
        header to getAssetById endpoint."
    - kind: added
      description: "Utils: Add assets content type and content disposition utils."
    - kind: added
      description: "Utils: Add functions for mime type operations and icons."
    - kind: changed
      description: "Block(form): Show the upload button to the right in repeated file inputs."
    - kind: changed
      description: "Block(form): Use the mime type of files in the file input to show
        a placeholder."
    - kind: changed
      description: "App: Make debug page path case insensitive."
    - kind: fixed
      description: "Block(form): Don't show the message for long submission if there
        are form errors."
    - kind: fixed
      description: "Server: Allow patching resources with assets by name."
  artifacthub.io/license: LGPL-3.0-only
  artifacthub.io/links: |
    - name: Appsemble Studio
      url: https://appsemble.app
    - name: Documentation
      url: https://appsemble.app/docs
    - name: support
      url: https://gitlab.com/appsemble/appsemble/-/issues
  artifacthub.io/recommendations: |
    - url: https://artifacthub.io/packages/container/appsemble/appsemble
    - url: https://artifacthub.io/packages/helm/cert-manager/cert-manager
    - url: https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx
  artifacthub.io/signKey: |
    fingerprint: C4EE8EE8B16EE94FEA3B0A7C346FF2248F310B53
    url: https://gitlab.com/appsemble-bot.gpg
apiVersion: v2
appVersion: 0.29.8
dependencies:
- - condition: postgresql.enabled
  name: postgresql
  repository: https://charts.bitnami.com/bitnami
  version: 11.6.3
description: The open source low-code app building platform
home: https://appsemble.com
icon: https://charts.appsemble.com/icon.svg
keywords:
- - app
- - apps
- - appsemble
- - framework
- - low-code
- - lowcode
kubeVersion: '>=1.21.0'
name: appsemble
sources:
- - https://gitlab.com/appsemble/appsemble
- - https://gitlab.com/appsemble/charts
type: application
version: 0.29.8

...
files:
  appsemble-0.29.8.tgz: sha256:27b90229ba564c852807b8aee5fb12959b9be9eb50882fa8d8a4fd343699c6f7
-----BEGIN PGP SIGNATURE-----

wsFcBAEBCgAQBQJmtHiZCRA0b/IkjzELUwAATicQAEv1JBuW3i1DHZAHyD9fAAQU
J6eynl/pjNN+enh5Ui6hIZmzSf9T3gXFpH+gYRsb5Szpe3LnhOpVJMaVcE++jSg1
qdO0qnQO/YriikhngLHbbh2LvuIPqhUOXzGANfCUGDkORbG8GdSo9n8i0N7Q0imj
w/Nhq/0VxOmA4Hagu6jxkw/q0FQmCmiTdmIDHD71I5I7zbVpt+YS1VR4ILco49i4
7z8OAoVgjq62700ftboDsBqPGLOYwN6upkOk9AC1BgPQbevtUj1zyhkxoYkf40De
fFLfbkoUVWynfxHSscwNVIyNpzozBX4otC4c7LP8WechdUtCKR0Jkh6PrPLgjhkl
PlQFFx9SlhV1IjmXKc0hl+lJJWScq7Ojuy0LfAf9lRoWjAKgAMY1Kjx9fUVsxfPP
nEAz256VbC2Qk3fcIKNfsHSBbf1jrVZIhrKqn5ZYVdT5ovorysK5p4BBKiHO3lk1
L+yVihCXMeDPg+QMekLbspemrzN065/T1HPENLQkbhq1i+B+GAoIiTpKEUr5MMLs
/3uga3mprDGhD5OU/zPmCumU6+ZyjThAoreBmVpWuJrLB9TvI3k73OWFB/LOWjOV
I1AAwnDScr6LDzP0H1lrOdtnStCgY9sD/ajw5yKt6g0ta1XT9A7Gvo2mKUTL7nb8
klMu43k+1fT5ZAW9aci/
=RcV9
-----END PGP SIGNATURE-----