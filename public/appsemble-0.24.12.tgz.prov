-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

annotations:
  artifacthub.io/changes: |
    - kind: added
      description: "Cli: Publish resources recursively in the app publish command
        based on resource references."
    - kind: added
      description: "Server: Reseed resources recursively in the /app/{id}/reseed
        endpoint based on resource references."
    - kind: added
      description: "Server: Reseed resources recursively in the
        cleanupResourcesAndAssets command based on resource references."
    - kind: changed
      description: "Node-utils: Patch resource schema in processResourceBody to allow
        resource references by index."
    - kind: changed
      description: "Utils: Added validation for resource names against the reserved
        keywords created, updated, author, editor, seed, ephemeral, clonable and
        expires."
    - kind: fixed
      description: "App: App bar rendering title wrong."
    - kind: fixed
      description: "Studio: Unexpected error on the organization docs page."
    - kind: fixed
      description: "Utils: Remove additional history stack in the remapper context."
  artifacthub.io/license: LGPL-3.0-only
  artifacthub.io/links: |
    - name: Appsemble Studio
      url: https://appsemble.app
    - name: Documentation
      url: https://appsemble.app/docs
    - name: support
      url: https://gitlab.com/appsemble/appsemble/-/issues
  artifacthub.io/recommendations: |
    - url: https://artifacthub.io/packages/container/appsemble/appsemble
    - url: https://artifacthub.io/packages/helm/cert-manager/cert-manager
    - url: https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx
  artifacthub.io/signKey: |
    fingerprint: C4EE8EE8B16EE94FEA3B0A7C346FF2248F310B53
    url: https://gitlab.com/appsemble-bot.gpg
apiVersion: v2
appVersion: 0.24.12
dependencies:
- - condition: postgresql.enabled
  name: postgresql
  repository: https://charts.bitnami.com/bitnami
  version: 11.6.3
description: The open source low-code app building platform
home: https://appsemble.com
icon: https://charts.appsemble.com/icon.svg
keywords:
- - app
- - apps
- - appsemble
- - framework
- - low-code
- - lowcode
kubeVersion: '>=1.21.0'
name: appsemble
sources:
- - https://gitlab.com/appsemble/appsemble
- - https://gitlab.com/appsemble/charts
type: application
version: 0.24.12

...
files:
  appsemble-0.24.12.tgz: sha256:f26f14885ae481cc9ecc6da85cc1f7937902aee0b6529c596c0ec970182c9d1c
-----BEGIN PGP SIGNATURE-----

wsFcBAEBCgAQBQJlwkOfCRA0b/IkjzELUwAAYYIQAFA+sXaR52NQ7wlpzgtECm+l
+Qi11l/ZGKINaXrjo4H/aOfM/eNnMsirUIUjS2VAZlPSHGzVEt30Ix9ZAb2HQ3fr
txW3gXRE9IMMNQd6iAx8FH9gs9d9fA669oy6ChY9Zdnpp0+CU+47Gv7mKWUQCjXc
fxFNUR60ymOQFS9Thyr3J+KZFn5SMw5udHpjeqcQClKX1cLaHxeuILcMoFQb+RMl
r+ST4wsdqhg90i41vw8Wp13ZSn36wgcwbWNxUm8qLDw0Bei/RtzLW6594Z+lQm7m
L3GMo0YvlNOwet4QTJpxUBGYUVAxhK0IAawde9lMLE96eZO+3bJt3du6BrYyiF4Z
Uhd9Uf79oPhvtflm6//pqruQ3J2KCCQu9HEAoRjoNhbGNCb6xykKeYdyzY70Xek8
BpsA8Fzd4bWRt6U4QIB4orxuAFev9oVO3aBF/xh9IeX3sONxyNPvikptzQqb9FHW
a1OGQ/pY5logtOSv9FTlsRN9X53zKL6Eqf71145nDpq9/1u98WQn/sgTN2ufURJw
biCbYgg17vVxE385JMLSLt1U8a0bjujDjraiYL9sI7db+GhuWEQ78OpR9/Ed/GtQ
49xY2OsbSuWwESMMFpE1qV1zrkvZ2kqWp0UN8MBcxqumtB9PUQ2ZvxMDDrs8OiwG
Yh/rgAD/lO9R70pURzUG
=CGTg
-----END PGP SIGNATURE-----