-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

annotations:
  artifacthub.io/changes: |
    - kind: added
      description: "Block(form): Add support for icon inside file entry."
    - kind: added
      description: "Block(form): Add support for icon to geocoordinates field."
    - kind: added
      description: "Block(form): Add support for icon to static field."
    - kind: added
      description: "Block(form): Add support for label to geocoordinates field."
    - kind: added
      description: "Block(form): Add support for tag to geocoordinates field."
    - kind: added
      description: "Block(form): Add support for “required‘ requirement to
        geocoordinates field."
    - kind: added
      description: "App: Remap query remapper on the client when request action
        requests are proxied."
    - kind: added
      description: "Preact-components: Add icon right option to render icon on the right."
    - kind: added
      description: "Preact-components: Add styling to dual slider when it has an icon."
    - kind: added
      description: "Preact-components: Add styling to slider when it has an icon."
    - kind: added
      description: "Preact-components: Add support for icon to dual slider field."
    - kind: added
      description: "Preact-components: Add support for icon to slider field."
    - kind: added
      description: "Studio: Add a button for mobile devices to open and show the app preview."
    - kind: added
      description: "Studio: Add option to pass down class name to the editor tab
        component`s elements."
    - kind: added
      description: "Studio: Add option to set the dropdown content container to the right side."
    - kind: changed
      description: "Block(form): Change icon implementation, convert Font Awesome icon
        to SVG instead."
    - kind: changed
      description: "Cli: Update definition to include changes."
    - kind: changed
      description: "React-components: Add an extra parameter for a reference to be
        exempt from triggering the closing event."
    - kind: changed
      description: "Server: Rename database model “Member” to “OrganizationMember”."
    - kind: changed
      description: "Studio: Breadcrumbs now hide on smaller mobile devices."
    - kind: changed
      description: "Studio: Change the style of tabs navigator to resize and apply
        styling based on screen size."
    - kind: changed
      description: "Studio: Replace normal buttons with a collapsible navigation bar
        for mobile user interface."
    - kind: changed
      description: "Types: Rename type “Member” to “OrganizationMember” in the code."
    - kind: fixed
      description: "Block(form): Fix default min and max value for range input field."
    - kind: fixed
      description: "Block(form): Fix list input field styling when icon is present."
    - kind: fixed
      description: "Block(form): Fix the file entry’s image source."
    - kind: fixed
      description: "React-components: Fix GUI path name matching condition."
  artifacthub.io/license: LGPL-3.0-only
  artifacthub.io/links: |
    - name: Appsemble Studio
      url: https://appsemble.app
    - name: Documentation
      url: https://appsemble.app/docs
    - name: support
      url: https://gitlab.com/appsemble/appsemble/-/issues
  artifacthub.io/recommendations: |
    - url: https://artifacthub.io/packages/container/appsemble/appsemble
    - url: https://artifacthub.io/packages/helm/cert-manager/cert-manager
    - url: https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx
  artifacthub.io/signKey: |
    fingerprint: C4EE8EE8B16EE94FEA3B0A7C346FF2248F310B53
    url: https://gitlab.com/appsemble-bot.gpg
apiVersion: v2
appVersion: 0.24.0
dependencies:
- - condition: postgresql.enabled
  name: postgresql
  repository: https://charts.bitnami.com/bitnami
  version: 11.6.3
description: The open source low-code app building platform
home: https://appsemble.com
icon: https://charts.appsemble.com/icon.svg
keywords:
- - app
- - apps
- - appsemble
- - framework
- - low-code
- - lowcode
kubeVersion: '>=1.21.0'
name: appsemble
sources:
- - https://gitlab.com/appsemble/appsemble
- - https://gitlab.com/appsemble/charts
type: application
version: 0.24.0

...
files:
  appsemble-0.24.0.tgz: sha256:5bc288eaeabf00f5cdfcb1317e409819adb016668465d48424b7ea9c9f60587c
-----BEGIN PGP SIGNATURE-----

wsFcBAEBCgAQBQJlpnL1CRA0b/IkjzELUwAAj/AQABy16yL+qWy1wUGogxUNoPz9
GmbNAzWSxm3ANaj4o9aNTE2kjLPzeaqMCVHDpBX4ACJnrVeZJLQh0n5O293zsGj6
6ALIJZZkypltFg4qMjgVoZn+o3EOGgcQVUeT1djJKrMi6hlUOmhfzGPDUsJEiofS
vaVdxa9HA1CjKIJfbpI9jav5EXre8aJeImtq5xgMVq3xsYNqb7Yu6bqlLuZFxyS5
jHt3g+KLlr9i+mszi3QWHuHCwRTx9tw/NMdFiDe4g0d9aCIV7K8mMWLUEBljZg/J
Xg/TpbUuKowIu7jUjzTGuAZhmj5vBqq5+391UE2+g5mkjxPsfbE6WDZlua8uy5R9
00F5iQeTR3AvDs69huEzyE3IyLRfKhI5QUOx+9piL60MnopBxHdiTV6bOns7ZHzg
iDSyyx/R3OJrvqJW7EFtObGyjyVREELj2xrnmdUKq4zgQz3LxydlHxSMu13oRmRX
TZyeh70INi5QW3e5QgdE//D5DtL9ZxaP+Xq5qquCnqauRWNykdFHmHyy3wSZGknn
jmhN/MMEmVihGVnpigV7V86uBq9YT8FAFkivvjppOfH9nYCRSozWNGh65hyY1c7G
BqCwli8uqC0pvqoK5J999X9hTEu58N1+/py9i6cUKok9i2rKrLyRFdxqMl+gwqM/
LwfbcamD/NM9em6S+fO2
=uaFz
-----END PGP SIGNATURE-----